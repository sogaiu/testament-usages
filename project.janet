(declare-project
  :name "testament-usages"
  :url "https://github.com/sogaiu/testament-usages"
  :repo "git+https://github.com/sogaiu/testament-usages"
  :dependencies ["https://github.com/pyrmont/testament"])
